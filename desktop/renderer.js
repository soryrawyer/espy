import { create } from "./file-table.js";
const { ipcRenderer } = require("electron");

/**
 * general process here:
 * 1) button clicked, showDialog called
 * 2) showDialog sends a message to the main process.
 *    this message essentially says "please open a file picker"
 * 3) the main process listens for an event to the "open-file-picker"
 *    ...queue, I guess? topic? channel?
 *    anyway, so the main process then opens the dialog box
 * 4) *only if the user picks a/some file/s*: the main process will
 *    reply to the "open-file-picker" topic
 * 5) the `ipcRenderer.on(...)` call knows that something's been updated,
 *    so let's refresh the file table!
 */

function showDialog() {
  ipcRenderer.send("open-file-picker");
}

function refresh() {
  const table = document.getElementsByTagName("file-table")[0];
  create(table);
}

ipcRenderer.on("open-file-picker", (event, arg) => {
  refresh();
});

document.getElementsByTagName("button")[0].onclick = showDialog;
