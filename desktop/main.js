// Modules to control application life and create native browser window
const {
  app,
  dialog,
  BrowserWindow,
  nativeTheme,
  ipcMain,
} = require("electron");
const fs = require("fs");
const mm = require("music-metadata");
const sqlite3 = require("sqlite3");
const config = require("./config.json");
const flatMap = require("array.prototype.flatmap");

function createWindow() {
  // Create the browser window.
  const mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true,
      nodeIntegrationInSubFrames: true,
      contextIsolation: false,
    },
  });

  nativeTheme.themeSource = "light";

  // and load the index.html of the app.
  mainWindow.loadFile("index.html");

  // Open the DevTools.
  mainWindow.webContents.openDevTools();
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(() => {
  createWindow();

  app.on("activate", function () {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) createWindow();
  });
});

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on("window-all-closed", function () {
  if (process.platform !== "darwin") app.quit();
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.

ipcMain.on("open-file-picker", (event, arg) => {
  // notes about return value:
  // array of file paths.
  // If the user selects multiple, then it'll have multiple entries
  // If the user just closes the box, the result is `undefined`
  // directories show up as one entry
  dialog
    .showOpenDialog({
      // it looks like maybe we can't have "openFile" and "openDirectory" together?
      // for now, I'm choosing to default to "directory"
      // since that seems the most useful
      properties: ["openDirectory", "multiSelections"],
    })
    .then((result) => {
      if (result.canceled) return;

      // expand any directories
      const files = resolveFilePaths(result.filePaths);
      return Promise.all(
        files.map((file) =>
          mm.parseFile(file).then((resp) => {
            return { common: resp.common, path: file };
          })
        )
      ).then((common) => common.map((c) => addToDb(c.common, c.path)));
    })
    .then(() => {
      event.reply("open-file-picker");
    })
    .catch((err) => console.log(err));
});

function resolveFilePaths(paths) {
  return flatMap(paths, (path) => {
    // for each path, return a path or list of paths
    if (fs.lstatSync(path).isDirectory()) {
      const subPaths = fs.readdirSync(path).map((entry) => `${path}/${entry}`);
      return resolveFilePaths(subPaths);
    } else {
      return [path];
    }
  });
}

const db = new sqlite3.Database(config.db_path);

function addToDb(common, path) {
  if (common.title === undefined) return;

  addOrUpdate(common, path);
}

function addOrUpdate(metadata, path) {
  // add new track or update existing track
  const fun = (err, rows) => {
    if (err) {
      return console.error(err);
    }

    if (rows.length === 0) {
      // insert new track and return
      const sql =
        "insert into tracks (title, release, artist, location) values (?, ?, ?, ?)";
      // > Object.keys(x);
      // [ 'track',
      // 'disk',
      // 'movementIndex',
      // 'title',
      // 'artists',
      // 'artist',
      // 'album',
      // 'year',
      // 'gapless',
      // 'encodedby',
      // 'picture' ]

      // > x.track
      // { no: null, of: null }

      // > x.disk
      // { no: null, of: null }

      db.run(
        sql,
        [metadata.title, metadata.album, metadata.artist, path],
        function (err) {
          if (err) {
            return console.error(err.message);
          }
        }
      );
      return;
    }

    const track = rows[0];
    if (
      metadata.title !== track.title ||
      metadata.album !== track.release ||
      metadata.artist !== track.artist
    ) {
      const sql = "update tracks set title = ?, release = ?, artist = ? where location = ?";
      db.run(sql, [metadata.title, metadata.album, metadata.artist, path], (err) => {
        if (err) {
          return console.error(err.message);
        }
      });
    } else {
      console.log(`nothin doin for ${path}`);
    }
  };
  db.all("select * from tracks where location = ?", path, fun);
}
