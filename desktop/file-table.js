const config = require("./config.json");
const sqlite = require("sqlite3");
const db = new sqlite.Database(config.db_path);

const template = document.createElement("template");
template.innerHTML = `
  <h4 class="header">
    <b>artists</b>
  </h4>
  <ul id="artist-list" class="column-container">
  </ul>

  <h4 class="header">
    <b>releases</b>
  </h4>
  <ul id="release-list" class="column-container">
  </ul>

  <h4 class="header">
    <b>tracks</b>
  </h4>
  <ul id="track-list" class="column-container">
  </ul>
`;

class FileTable extends HTMLElement {
  connectedCallback() {
    create(this);
  }
}

export function create(element) {
  element.innerHTML = template.innerHTML;
  getTracks();
}

customElements.define("file-table", FileTable);

function updateAudioHeader(artist, release, title) {
  const artistDiv = document.getElementById("audio-artist");
  artistDiv.textContent = `artist: ${artist}`;
  const releaseDiv = document.getElementById("audio-release");
  releaseDiv.textContent = `release: ${release}`;
  const trackDiv = document.getElementById("audio-track");
  trackDiv.textContent = `title: ${title}`;
}

function getTracks() {
  db.all("select * from tracks order by artist, release, title asc", (err, rows) => {
    if (err) {
      return console.error(err.message);
    }
    rows.forEach((row, index) => addTrackItems(row, index));
  });
}

function addTrackItems(track, index) {
  const artists = document.getElementById("artist-list");
  const releases = document.getElementById("release-list");
  const tracks = document.getElementById("track-list");

  artists.appendChild(createLiFromText(track.artist, index));
  releases.appendChild(createLiFromText(track.release, index));
  const trackCell = createLiFromText(track.title, index);
  trackCell.setAttribute("data-location", track.location);

  // when a user double clicks on a track name, set that track location
  // to be the audio element's src attribute and update the page
  // accordingly
  trackCell.addEventListener("dblclick", (ev) => {
    const audio = document.getElementById("player");
    audio.setAttribute("src", trackCell.dataset.location);
    updateAudioHeader(track.artist, track.release, track.title);
  });
  tracks.appendChild(trackCell);
}

function createLiFromText(text, index) {
  const li = document.createElement("li");
  li.classList.add("cell");
  li.style.gridRow = index + 1;

  const p = document.createElement("p");
  p.innerText = text;
  li.appendChild(p);
  return li;
}
