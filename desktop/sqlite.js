// import { IAudioMetadata } from "music-metadata/lib/type";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function () { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function () { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var _this = this;
var mm = require("music-metadata");
var util = require("util");
var sqlite3 = require("sqlite3").verbose();
var AUDIO_PATH = "/home/rory/media/audio/pinchy/4913/Mistery_Man-6050.mp3";
var db = new sqlite3.Database("./db/espy.db");
// db.run("drop table tracks");
// db.close();
// db.run("create table tracks (title TEXT, release TEXT, artist TEXT, location TEXT)");
// TODO: check if the tracks table is already present
(function () {
    return __awaiter(_this, void 0, void 0, function () {
        var common, sql;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, mm.parseFile(AUDIO_PATH)];
                case 1:
                    common = (_a.sent()).common;
                    console.log(common);
                    sql = "insert into tracks (title, release, artist, location) values (?, ?, ?, ?)";
                    db.run(sql, [common.title, common.album, common.artist, AUDIO_PATH], function (err) {
                        if (err) {
                            return console.error(err.message);
                        }
                        console.log("rows inserted: " + this.changes);
                    });
                    db.all("select * from tracks", [], function (err, rows) {
                        if (err) {
                            return console.error(err);
                        }
                        rows.forEach(function (row) {
                            console.log(row);
                        });
                    });
                    // {
                    //     "title": common.title,
                    //     "album": common.album,
                    //     "artist": common.artist
                    // }
                    // let stmt = db.prepare("insert into tracks values ()");
                    // stmt.run(common.title);
                    // stmt.run(common.album);
                    // stmt.run(common.artist);
                    // stmt.finalize();
                    // db.run(stmt);
                    // write info to db
                    db.close();
                    return [2 /*return*/];
            }
        });
    });
})();
// db.close();
// const f = (resp: IAudioMetadata) => console.log(util.inspect(resp, { showHidden: false, depth: null }));
// mm.parseFile(AUDIO_PATH).then(resp => f(resp))
/*
{ format:
   { tagTypes: [ 'ID3v2.2' ],
     trackInfo: [],
     lossless: false,
     container: 'MPEG',
     codec: 'MPEG 1 Layer 3',
     sampleRate: 44100,
     numberOfChannels: 2,
     bitrate: 192000,
     codecProfile: 'CBR',
     numberOfSamples: 170445312,
     duration: 3864.97306122449 },
  native:
   { 'ID3v2.2':
      [ { id: 'TT2', value: 'Mistery Man' },
        { id: 'TP1', value: 'Timedream' },
        { id: 'TAL', value: 'www.pinchyandfriends.com' },
        { id: 'TYE', value: '2015' },
        { id: 'COM:iTunPGAP', value: '0' },
        { id: 'TEN', value: 'iTunes 12.1.2.27' },
        { id: 'COM:iTunNORM',
          value:
           ' 00000B99 00000B99 00013897 00013897 001CD792 001CD792 00008539 00008539 00308B73 00308B73' },
        { id: 'COM:iTunSMPB',
          value:
           ' 00000000 00000210 00000774 000000000A28FF7C 00000000 058773EC 00000000 00000000 00000000 00000000 00000000 00000000' },
        { id: 'PIC',
          value:
           { format: 'image/jpeg',
             type: 'Other',
             description: '',
             data:
              <Buffer ff d8 ff e1 23 fc 45 78 69 66 00 00 4d 4d 00 2a 00 00 00 08 00 07 01 12 00 03 00 00 00 01 00 01 00 00 01 1a 00 05 00 00 00 01 00 00 00 62 01 1b 00 05 ... > } } ] },
*/
