// module for the custom audio element

const template = document.createElement("template");
template.innerHTML = `
<audio id="player"></audio>

<div id="controls">
  <button class="button" id="play">play</button>
  <button class="button" id="pause">pause</button>
</div>
<div id="progress">
  <div id="bar"></div>
</div>
`;

export class EspyAudio extends HTMLElement {
  connectedCallback() {
    create(this);
  }
}

function create(el) {
  el.innerHTML = template.innerHTML;
  initHandlers();
}

customElements.define("espy-audio", EspyAudio);

function initHandlers() {
  const audio = document.getElementById("player");
  const play = document.getElementById("play");
  const pause = document.getElementById("pause");
  const bar = document.getElementById("bar");
  const progress = document.getElementById("progress");

  pause.style.display = "none";
  play.style.display = "block";

  play.onclick = () => {
    audio.play();
    play.style.display = "none";
    pause.style.display = "block";
  };

  pause.onclick = () => {
    audio.pause();
    pause.style.display = "none";
    play.style.display = "block";
  };

  audio.ontimeupdate = () => {
    bar.style.width = parseInt(((audio.currentTime / audio.duration) * 100), 10) + "%";
  };

  progress.addEventListener("click", (event) => {
    const clickPosition = (event.pageX - progress.offsetLeft) / progress.offsetWidth;
    const clickTime = clickPosition * audio.duration;
    audio.currentTime = clickTime;
  });
}
